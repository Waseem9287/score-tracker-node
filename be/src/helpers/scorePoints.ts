export function getScorePoints(players: number, place: number) {
  const playersPointsMatrix = [
    null,
    [25, -25],
    [35, 15, -25],
    [35, 15, 0, -25],
    [50, 25, 0, -25, -25],
    [50, 25, 10, 0, -25, -25],
    [50, 25, 10, 0, 0, -25, -25],
    [50, 25, 10, 0, 0, 0, -25, -25],
    [50, 25, 10, 5, 0, 0, 0, -25, -25],
    [50, 25, 15, 5, 5, 0, 0, 0, -25, -25],
  ];

  return playersPointsMatrix[players - 1][place - 1];
}