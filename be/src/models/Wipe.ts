import { Document, Model, model, Schema } from "mongoose";
import { IPlayer } from './Player';

/**
 * Interface to model the Player Schema for TypeScript.
 * @param players:ref => Player
 */
export interface IWipe extends Document {
  player: IPlayer["_id"];
}

const wipeSchema: Schema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  player: {
    type: Schema.Types.ObjectId,
    ref: "Player"
  },
});

// @ts-ignore
const Wipe: Model<IWipe> = model("Session", wipeSchema);

export default Wipe;
