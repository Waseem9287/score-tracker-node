import { Document, Model, model, Schema } from "mongoose";
import { IPlayer } from './Player';
import { IPlace } from './Place';
import { ISeason } from './Season';

/**
 * Interface to model the Player Schema for TypeScript.
 * @param season:ref => Season
 * @param players:ref => Player
 * @param places:ref => Place
 */
export interface ISession extends Document {
  season: ISeason["_id"];
  players: Array<IPlayer["_id"]>;
  places: Array<IPlace["_id"]>;
}

const sessionSchema: Schema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  season: {
    type: Schema.Types.ObjectId,
    ref: "Season"
  },
  players: [{
    type: Schema.Types.ObjectId,
    ref: "Player"
  }],
  places: [{
    type: Schema.Types.ObjectId,
    ref: "Place"
  }],
});

// @ts-ignore
const Session: Model<ISession> = model("Session", sessionSchema);

export default Session;
