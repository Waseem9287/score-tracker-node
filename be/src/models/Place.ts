import { Document, Model, model, Schema } from "mongoose";
import { IPlayer } from './Player';
import { ISession } from './Session';

/**
 * Interface to model the Player Schema for TypeScript.
 * @param players:ref => Player
 * @param session:ref => Session
 * @param place:number
 * @param scoreUpdater:number
 */
export interface IPlace extends Document {
  player: IPlayer["_id"];
  session: ISession["_id"];
  place: number;
  scoreUpdater: number;
}

const placeSchema: Schema = new Schema({
  date: {
    type: Date,
    default: Date.now
  },
  player: {
    type: Schema.Types.ObjectId,
    ref: "Player"
  },
  session: {
    type: Schema.Types.ObjectId,
    ref: "Session"
  },
  place: {
    type: Number,
    required: true,
  },
  scoreUpdater: {
    type: Number,
    required: true,
  }
});

// @ts-ignore
const Place: Model<IPlace> = model("Place", placeSchema);

export default Place;
