import { Document, Model, model, Schema } from "mongoose";
import { ISession } from './Session';

/**
 * Interface to model the Player Schema for TypeScript.
 * @param name:string
 * @param sessionsHistory:ref => Season
 */
export interface ISeason extends Document {
  name: string;
  sessionsHistory: Array<ISession["_id"]>;
}

const seasonSchema: Schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    default: Date.now
  },
  sessionsHistory: [{
    type: Schema.Types.ObjectId,
    ref: "Player"
  }],
});

// @ts-ignore
const Season: Model<ISeason> = model("Season", seasonSchema);

export default Season;
