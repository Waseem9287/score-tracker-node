import { Document, Model, model, Schema } from "mongoose";
import { ISession } from './Session';
import { IWipe } from './Wipe';

/**
 * Interface to model the Player Schema for TypeScript.
 * @param nickname:string
 * @param score:number
 * @param sessionsHistory:ref => Session
 * @param wipeHistory:ref => Wipe
 */
export interface IPlayer extends Document {
  nickname: string;
  score: number;
  sessionsHistory: Array<ISession["_id"]>;
  wipeHistory: Array<IWipe["_id"]>;
}

const playerSchema: Schema = new Schema({
  nickname: {
    type: String,
    required: true,
    unique: true
  },
  score: {
    type: Number,
    required: false,
    default: 300
  },
  date: {
    type: Date,
    default: Date.now
  },
  sessionsHistory: [{
    type: Schema.Types.ObjectId,
    ref: "Session"
  }],
  wipeHistory: [{
    type: Schema.Types.ObjectId,
    ref: "Wipe"
  }]
});

// @ts-ignore
const Player: Model<IPlayer> = model("Player", playerSchema);

export default Player;
