import bodyParser from "body-parser";
import express from "express";
const cors = require('cors');

import connectDB from "../config/database";
import player from "./routes/api/player";
import season from "./routes/api/season";
import session from "./routes/api/session";
import place from "./routes/api/place";

const app = express();

// Connect to MongoDB
connectDB();

// Express configuration
app.set("port", process.env.PORT || 5000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors({
  origin: 'http://localhost:8080'
}));


// @route   GET /
// @desc    Test Base API
// @access  Public
app.get("/", (_req, res) => {
  res.send("API Running");
});

app.use("/api/player", player);
app.use("/api/season", season);
app.use("/api/session", session);
app.use("/api/place", place);

const port = app.get("port");
const server = app.listen(port, () =>
  console.log(`Server started on port ${port}`)
);

export default server;
