import Request from '../types/Request';
import { Response } from 'express';
import { validationResult } from 'express-validator/check';
import HttpStatusCodes from 'http-status-codes';
import Season, { ISeason } from '../models/Season';
import { ISession } from '../models/Session';

export default class SeasonService {
  static async create(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    const { name } = req.body;

    try {
      // Create
      // Build profile object based on ISeason
      const seasonFields = {
        name,
        sessionsHistory: new Array<ISession>()
      };

      const season = new Season(seasonFields);

      await season.save();

      res.json(season);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }

  static async delete() {

  }

  static async get(req: Request, res: Response) {
    try {
      const season: ISeason = await Season.findOne({
        season: req.params.seasonId,
      });

      if (!season)
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: "Season not found" });

      res.json(season);
    } catch (err) {
      console.error(err.message);
      if (err.kind === "ObjectId") {
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: "Season not found" });
      }
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }

  static async getAll(_req: Request, res: Response) {
    try {
      const seasons = await Season.find();
      res.json(seasons);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }
}