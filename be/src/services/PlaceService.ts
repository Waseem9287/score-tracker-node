import Request from '../types/Request';
import { Response } from 'express';
import { validationResult } from 'express-validator/check';
import HttpStatusCodes from 'http-status-codes';
import Place, { IPlace } from '../models/Place';
import Season from "../models/Season";

export default class PlaceService {
  static async create(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    const { playerId, place, scoreUpdater } = req.body;

    try {
      // Build profile object based on IPlayer
      const placeFields = {
        playerId,
        place,
        scoreUpdater,
      };

      const placeObject = new Place(placeFields);

      await placeObject.save();

      res.json(placeObject);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }

  static async delete() {

  }

  static async getAll(_req: Request, res: Response) {
    try {
      const places = await Place.find();
      res.json(places);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }

  static async get(req: Request, res: Response) {
    try {
      const place: IPlace = await Place.findById(req.params.placeId);

      if (!place)
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: 'Place not found' });

      res.json(place);
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: 'Profile not found' });
      }
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }
}