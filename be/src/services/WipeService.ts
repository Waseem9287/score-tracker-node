import Request from '../types/Request';
import { Response } from 'express';
import { validationResult } from 'express-validator/check';
import HttpStatusCodes from 'http-status-codes';
import { IWipe } from '../models/Wipe';
import { ISession } from '../models/Session';
import Player, { IPlayer } from '../models/Player';

export default class WipeService {
  static async create(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    const { nickname } = req.body;

    try {
      // Create

      // Build profile object based on IPlayer
      const playerFields = {
        nickname,
        score: 300,
        sessionsHistory: new Array<ISession>(),
        wipeHistory: new Array<IWipe>(),
      };

      const player = new Player(playerFields);

      await player.save();

      res.json(player);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }
}