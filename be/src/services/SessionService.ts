import Request from '../types/Request';
import { Response } from 'express';
import { validationResult } from 'express-validator/check';
import HttpStatusCodes from 'http-status-codes';
import Session, { ISession } from '../models/Session';
import Place, { IPlace } from '../models/Place';
import Season from '../models/Season';
import Player from '../models/Player';
import { getScorePoints } from '../helpers/scorePoints';

export default class SessionService {
  static async create(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    try {
      const { season, players } = req.body;

      // Build profile object based on IPlayer
      const sessionFields = {
        season,
        players,
        places: new Array<IPlace>(),
      };

      const session = new Session(sessionFields);

      await session.save();

      await Season.updateOne({ '_id': session.season }, { $push: { sessionsHistory: session._id } });
      await Player.updateMany({ '_id': session.players }, { $push: { sessionsHistory: session._id } });

      res.json(session);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }

  static async delete() {

  }

  static async get(req: Request, res: Response) {
    try {
      const session: ISession = await Session.findOne({
        session: req.params.sessionId,
      });

      if (!session)
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: 'Session not found' });

      res.json(session);
    } catch (err) {
      console.error(err.message);
      if (err.kind === 'ObjectId') {
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: 'Session not found' });
      }
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }

  static async getAll(_req: Request, res: Response) {
    try {
      const sessions = await Session.find();
      res.json(sessions);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }

  static async kickPlayer(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    try {
      const { player, sessionId } = req.body;

      const session = await Session.findById(sessionId);
      const playersQuantity = session.players.length;
      const place = playersQuantity - session.places.length;
      const scoreUpdater = getScorePoints(playersQuantity, place);

      const placeObj = new Place({
        player,
        session: sessionId,
        place,
        scoreUpdater
      });

      await placeObj.save();

      const currentPlayer = await Player.findById(placeObj.player);

      await Session.updateOne({ '_id': placeObj.session }, { $push: { places: placeObj._id } });
      await Player.updateOne({ '_id': placeObj.player }, { $set: { score: currentPlayer.score + scoreUpdater }});

      return res.json(await Session.findById(sessionId));
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send('Server Error');
    }
  }
}