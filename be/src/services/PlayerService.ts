import Request from '../types/Request';
import { Response } from 'express';
import { validationResult } from 'express-validator/check';
import HttpStatusCodes from 'http-status-codes';
import { IWipe } from '../models/Wipe';
import { ISession } from '../models/Session';
import Player, { IPlayer } from '../models/Player';

export default class PlayerService {
  static async create(req: Request, res: Response) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(HttpStatusCodes.BAD_REQUEST)
        .json({ errors: errors.array() });
    }

    const { nickname } = req.body;

    try {
      // Build profile object based on IPlayer
      const playerFields = {
        nickname,
        score: 300,
        sessionsHistory: new Array<ISession>(),
        wipeHistory: new Array<IWipe>(),
      };

      const player = new Player(playerFields);

      await player.save();

      res.json(player);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }

  static async delete() {

  }

  static async get(req: Request, res: Response) {
    try {
      const player: IPlayer = await Player.findOne({
        player: req.params.playerId,
      });

      if (!player)
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: "Player not found" });

      res.json(player);
    } catch (err) {
      console.error(err.message);
      if (err.kind === "ObjectId") {
        return res
          .status(HttpStatusCodes.NOT_FOUND)
          .json({ msg: "Player not found" });
      }
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }

  static async getAll(_req: Request, res: Response) {
    try {
      const players = await Player.find();
      res.json(players);
    } catch (err) {
      console.error(err.message);
      res.status(HttpStatusCodes.INTERNAL_SERVER_ERROR).send("Server Error");
    }
  }
}