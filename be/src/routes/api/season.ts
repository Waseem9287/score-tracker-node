import { Router } from "express";
import { check } from "express-validator/check";
import SeasonService from '../../services/SeasonService';

const router: Router = Router();

// @route   POST api/season
// @desc    Create or update season
// @access  Public
router.post(
  "/",
  [
    check("name", "Name is required").not().isEmpty(),
  ],
  SeasonService.create
);

// @route   GET api/season
// @desc    Get all seasons
// @access  Public
router.get("/", SeasonService.getAll);

// @route   GET api/season/:seasonId
// @desc    Get season by seasonId
// @access  Public
router.get("/:seasonId", SeasonService.get);

// @route   DELETE api/season
// @desc    Delete season
// @access  Public
router.delete("/", SeasonService.delete);

export default router;
