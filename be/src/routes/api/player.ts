import { Router } from "express";
import { check } from "express-validator/check";
import PlayerService from '../../services/PlayerService';
import WipeService from '../../services/WipeService';

const router: Router = Router();

// @route   POST api/player
// @desc    Create or update players info
// @access  Public
router.post(
  "/",
  [
    check("nickname", "Nickname is required").not().isEmpty(),
  ],
  PlayerService.create,
);

// @route   POST api/player/wipe/:playerId
// @desc    wipePlayer by playerId
// @access  Public
router.post("/wipe/:playerId", WipeService.create);

// @route   GET api/player
// @desc    Get all players
// @access  Public
router.get("/", PlayerService.getAll);

// @route   GET api/player/:playerId
// @desc    Get profile by userId
// @access  Public
router.get("/:playerId", PlayerService.get);

// @route   DELETE api/player
// @desc    Delete player
// @access  Public
router.delete("/", PlayerService.delete);

export default router;
