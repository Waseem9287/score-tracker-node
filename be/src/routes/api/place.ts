import { Router } from "express";
import PlaceService from "../../services/PlaceService";

const router: Router = Router();


// @route   GET api/place
// @desc    Get all places
// @access  Public
router.get("/", PlaceService.getAll);

// @route   GET api/place/:placeId
// @desc    Get place by placeId
// @access  Public
router.get("/:seasonId", PlaceService.get);


export default router;
