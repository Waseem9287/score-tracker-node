import { Router } from "express";
import { check } from "express-validator/check";
import SessionService from '../../services/SessionService';

const router: Router = Router();

// @route   POST api/session
// @desc    Create or update session
// @access  Public
router.post(
  "/",
  [
    check("players", "Players is required").not().isEmpty(),
  ],
  SessionService.create
);

// @route   POST api/session/kick_player
// @desc    Kick player from session
// @access  Public
router.post("/kick_player", SessionService.kickPlayer);

// @route   GET api/session
// @desc    Get all sessions
// @access  Public
router.get("/", SessionService.getAll);

// @route   GET api/season/:sessionId
// @desc    Get session by sessionId
// @access  Public
router.get("/:sessionId", SessionService.get);

// @route   DELETE api/session
// @desc    Delete session
// @access  Public
router.delete("/", SessionService.delete);

export default router;
