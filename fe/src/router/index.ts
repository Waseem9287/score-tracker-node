import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import MainPage from '../views/MainPage.vue'
import PlayerInfo from '../views/PlayerInfo.vue'
import GameInfo from '../views/GameInfo.vue'

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'main',
    component: MainPage
  },
  {
    path: '/player/:id',
    name: 'Player info',
    component: PlayerInfo
  },
  {
    path: '/session/:id',
    name: 'Session info',
    component: GameInfo
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
