import Vue from 'vue'
import Vuex from 'vuex'
import {reqGetData} from "@/helpers/apiHelper";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        players: {},
        sessions: {},
        seasons: {},
        places: {},
        loadStatus: 0,
    },
    getters: {
        getPlayers(state) {
            return state.players;
        },
        getSessions(state) {
            return state.sessions;
        },
        getSeasons(state) {
            return state.seasons;
        },
        getPlaces(state) {
            return state.places;
        },
        loaded(state) {
            return state.loadStatus >= 4;
        }
    },
    mutations: {
        SET_PLAYERS(state, players) {
            state.players = players;
        },
        SET_SESSIONS(state, sessions) {
            state.sessions = sessions;
        },
        SET_SEASONS(state, seasons) {
            state.seasons = seasons;
        },
        SET_PLACES(state, places) {
            state.places = places;
        },
        INCR_LOAD(state) {
            state.loadStatus++;
        },
        SET_LOAD(state, value) {
            state.loadStatus = value;
        }
    },
    actions: {
        async reqPlayers({commit}) {
            commit('SET_PLAYERS', await reqGetData('/api/player'));
            commit('INCR_LOAD');
        },
        async reqSessions({commit}) {
            commit('SET_SESSIONS', await reqGetData('/api/session'));
            commit('INCR_LOAD');
        },
        async reqSeasons({commit}) {
            commit('SET_SEASONS', await reqGetData('/api/season'));
            commit('INCR_LOAD');
        },
        async reqPlaces({commit}) {
            commit('SET_PLACES', await reqGetData('/api/place'));
            commit('INCR_LOAD');
        },
        async reqData({ dispatch, commit }) {
            commit('SET_LOAD', 0);

            await Promise.all([
                dispatch('reqPlayers'),
                dispatch('reqSessions'),
                dispatch('reqSeasons'),
                dispatch('reqPlaces'),
            ]);
        }
    },
})
