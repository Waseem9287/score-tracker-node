import Axios from "@/helpers/axios";

export interface HashTable<T> {
    [key: string]: T;
}

export interface IReqData {
    _id: string;
}

export function parseFromArrayToHash<T extends IReqData>(data: Array<T>): HashTable<T> {
    return data.reduce((acc, currentValue) => {
        acc[currentValue._id] = currentValue;

        return acc;
    }, {} as HashTable<T>);
}

export async function reqGetData(route: string) {
    const response = await Axios.get(route);

    return parseFromArrayToHash(response.data);
}