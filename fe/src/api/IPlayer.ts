import {IReqData} from "@/helpers/apiHelper";

export interface IPlayer extends IReqData {
    __v: number;
    _id: string;
    date: string;
    nickname: string;
    score: number;
    sessionsHistory: Array<string>
    wipeHistory: Array<string>
}